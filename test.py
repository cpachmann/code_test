from typedb.driver import TypeDB, SessionType, TransactionType
import os
from .openai_auditor import OpenAI_Auditor
from . import audit_data_ingestion
from typing import List
import re

class FileAuditor:
    print("Start file auditing...")
    @staticmethod
    def fetch_files_from_repo(git_repo_url, audit_uuid):
        file_paths = []
        with TypeDB.core_driver(TypeDB.DEFAULT_ADDRESS) as driver:
            with driver.session("CodeDD_test_2", SessionType.DATA) as session:
                with session.transaction(TransactionType.READ) as tx:
                    print("Querying TypeDB for file paths...")
                    fetch_query = f'''
                    match
                        $audit isa audit, has audit_uuid "{audit_uuid}";
                        $repo isa git_repo, has url "{git_repo_url}";
                        $root_dir isa root_directory;
                        $file isa file, has file_path $path;
                        (host: $repo, hosted: $root_dir) isa repository_host;
                        (directory: $root_dir, file: $file) isa directory_content;
                    get $path;
                    '''
                    responses = tx.query.match(fetch_query)
                    for response in responses:
                        file_path = response.get("path").as_string()
                        file_paths.append(file_path)
                
        return file_paths

    @staticmethod
    def read_file_content_from_cache(cache_dir, file_path):
        adjusted_file_path = file_path.split('/')[-1]
        full_file_path = os.path.join(cache_dir, adjusted_file_path)
        if os.path.exists(full_file_path):
            with open(full_file_path, 'r', encoding='utf-8') as f:
                content = f.read()
            return content
        else:
            raise FileNotFoundError(f"File not found in cache: {full_file_path}")
        
    @staticmethod
    def filter_code_files(file_paths: List[str]) -> List[str]:
        """
        Filters out documentation, license, data, and other non-code files.
        """
        # Define patterns for files to exclude
        excluded_patterns = [
            # Documentation and data files
            r".*\.(md|rst|pdf|csv|xls|xlsx|json|xml|jpg|jpeg|png|gif|bmp|tiff|svg|psd)$",
            # License and readme files
            r".*LICENSE.*",
            r".*README.*",
            # Binary and executable files
            r".*\.(exe|dll|so|bin|o|a)$",
            # Archive files
            r".*\.(zip|tar|gz|bz2|7z|rpm|deb)$",
            # Certificate files
            r".*\.(pem|crt|key|pfx|cer)$",
            # Multimedia files
            r".*\.(mp3|wav|mp4|avi|mkv|flv|mov)$",
            # Miscellaneous files
            r".*\.(log|out|err|bak|tmp|temp|cache)$",
            # Add patterns for other non-code files as needed
        ]
       
        # Compile the patterns to improve performance
        excluded_regexes = [re.compile(pattern, re.IGNORECASE) for pattern in excluded_patterns]

        # Filter out files that match any of the excluded patterns
        included_files = [path for path in file_paths if not any(regex.match(path) for regex in excluded_regexes)]

        return included_files

    @staticmethod
    def ai_auditing(file_paths, cache_dir):
        print("Initializing OpenAI Auditor...")
        auditor = OpenAI_Auditor()
        success = True

        code_file_paths = FileAuditor.filter_code_files(file_paths)

        if not code_file_paths:
            print("No code files to audit.")
            return False

        print("Files to be audited:", code_file_paths)

        for file_path in code_file_paths:
            print(f"Auditing file: {file_path}")
            try:
                file_content = FileAuditor.read_file_content_from_cache(cache_dir, file_path)
                response = auditor.audit_content(file_content)
                print(f"Starting audit data ingestion for {file_path}...")
                audit_data_ingestion.ingest_audit_data_to_typedb(response, file_path)
            except FileNotFoundError as e:
                print(e)
                success = False 

        if success:
            print("Auditing completed successfully.")
        else:
            print("Auditing completed with some errors.")

        return success

